package run

import (
	"context"
	"database/sql"
	"fmt"
	"geoservicegrpc/proxy/config"
	"geoservicegrpc/proxy/internal/entities/authEntity"
	"geoservicegrpc/proxy/internal/infrastructure/errors"
	"geoservicegrpc/proxy/internal/infrastructure/responder"
	"geoservicegrpc/proxy/internal/infrastructure/server"
	"geoservicegrpc/proxy/internal/modules"
	service2 "geoservicegrpc/proxy/internal/modules/geo/service"
	"geoservicegrpc/proxy/route"
	"geoservicegrpc/proxy/rpc/geo"
	"geoservicegrpc/proxy/rpc/grpc/proto"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth"
	"github.com/redis/go-redis/v9"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"net/rpc"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(option ...interface{}) Runner
}

// App - структура приложения
type App struct {
	Conf     config.Config
	Srv      server.Server
	Rpc      server.Server
	Sig      chan os.Signal
	Services *modules.Services
}

func NewApp(conf config.Config) *App {
	return &App{
		Conf: conf,
		Sig:  make(chan os.Signal, 1),
	}
}

// Run - запуск приложения
func (a *App) Run() int {

	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		log.Println("signal interrupt recieved", sigInt)
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.Srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError

}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {

	//postgresql
	dsn := a.Conf.GetDsnDB()
	fmt.Println("dsn", dsn)

	db, err := sql.Open(a.Conf.DB.Driver, dsn)
	if err != nil {
		fmt.Println("error sql.Open")
		log.Fatal(err)
	}

	// инициализация менеджера ответов сервера
	respond := responder.NewResponder()

	//вот здесь запустим редис для быстрого кэширования запросов
	clientRedis := redis.NewClient(&redis.Options{
		Addr: "redis:6379",
		DB:   0,
	})
	ctx := context.Background()
	// Проверка соединения с Redis
	pong, err := clientRedis.Ping(ctx).Result()
	if err != nil {
		fmt.Println("Ошибка соединения с Redis:", err)
		log.Println(err)
	}
	fmt.Println("Соединение с Redis успешно:", pong)

	//авторизация: инициализация токена
	authStorage := &authEntity.StorageAuth{UsersMap: map[string]authEntity.User{}}
	token := jwtauth.New("HS256", []byte("randomPass"), nil)

	//инициализируем сервисы
	services := modules.NewServices(db, clientRedis, &a.Conf, authStorage, token)
	a.Services = services

	// инициализация сервиса Geo в RPC
	geoRPC := geo.NewGeoServiceRPC(a.Services.Geo)
	serverGRPC := grpc.NewServer()
	serverRPC := rpc.NewServer()
	err = serverRPC.Register(geoRPC)
	if err != nil {
		log.Fatal("error init geo RPC", err)
	}

	// инициализация сервера RPC
	switch a.Conf.ServerRPC.Type {
	case "rpc":
		a.Rpc = server.NewServerRPC(a.Conf.ServerRPC, serverRPC)
	case "jsonrpc":
		a.Rpc = server.NewServerJsonRPC(a.Conf.ServerRPC, serverRPC)
	case "grpc":
		a.Rpc = server.NewServerGRPC(a.Conf.ServerRPC, serverGRPC)
		geoGRPC := geo.NewGeoServiceGRPC(services.Geo)
		//регистрация geo в grpc-сервере
		proto.RegisterGeoServiceRPCServer(serverGRPC, geoGRPC)
	}

	go func() {
		err = a.Rpc.Serve(context.Background())
		if err != nil {
			log.Fatal("app: server error", err)
		}
	}()

	// инициализация сервиса(а в нём и клиента) для взаимодействия с сервисом геосервиса
	var serviceClient service2.GeoServicer
	switch a.Conf.ServerRPC.Type {
	case "rpc":
		serviceClient = service2.NewGeoServicRPC(a.Conf)
	case "jsonrpc":
		serviceClient = service2.NewGeoServicJsonRPC(a.Conf)
	case "grpc":
		serviceClient = service2.NewGeoServicGRPC(a.Conf)
	default:
		log.Println("invalid rpc protocol")
	}

	controllers := modules.NewControllers(a.Services, serviceClient, respond)

	// инициализация роутера
	r := chi.NewRouter()
	router := route.NewApiRouter(r, controllers, token)

	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.Conf.Server.Port),
		Handler: router,
	}

	// инициализация сервера
	a.Srv = server.NewHttpServer(srv)
	// возвращаем приложение
	return a

}
