package main

import (
	"geoservicegrpc/proxy/config"
	"geoservicegrpc/proxy/run/app"
	"os"
)

func main() {
	conf := config.NewConfig()

	app := run.NewApp(conf)

	exitCode := app.
		// Инициализируем приложение
		Bootstrap().
		// Запускаем приложение
		Run()
	os.Exit(exitCode)

}
