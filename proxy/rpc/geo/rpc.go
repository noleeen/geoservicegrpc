package geo

import (
	"geoservicegrpc/proxy/internal/entities/geoEntity"
	"geoservicegrpc/proxy/internal/modules/geo/service"
)

type GeoServiceRPC struct {
	GeoService service.GeoServicer
}

func NewGeoServiceRPC(g service.GeoServicer) *GeoServiceRPC {
	return &GeoServiceRPC{GeoService: g}
}

func (g *GeoServiceRPC) PrepareGeocodeRequest(in geoEntity.GeocodeRequest, out *geoEntity.Suggestions) error {
	req, err := g.GeoService.PrepareGeocodeRequest(in)
	if err != nil {
		return err
	}
	*out = *req
	return nil
}
func (g *GeoServiceRPC) PrepareSearchRequest(in geoEntity.SearchRequest, out *geoEntity.SearchResponse) error {
	req, err := g.GeoService.PrepareSearchRequest(in)
	if err != nil {
		return err
	}
	*out = *req
	return nil
}
