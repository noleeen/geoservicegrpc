package geo

import (
	"context"
	"geoservicegrpc/proxy/internal/entities/geoEntity"
	"geoservicegrpc/proxy/internal/modules/geo/service"
	"geoservicegrpc/proxy/rpc/grpc/proto"
)

type GeoServicerGRPC interface {
	PrepareGeocodeRequest(c context.Context, request *proto.GeocodeRequest) (*proto.Suggestions, error)
	PrepareSearchRequest(c context.Context, request *proto.SearchRequest) (*proto.SearchResponse, error)
}

type GeoServiceGRPC struct {
	GeoService service.GeoServicer
	proto.UnimplementedGeoServiceRPCServer
}

func NewGeoServiceGRPC(g service.GeoServicer) *GeoServiceGRPC {
	return &GeoServiceGRPC{GeoService: g}
}

func (g *GeoServiceGRPC) PrepareGeocodeRequest(c context.Context, request *proto.GeocodeRequest) (*proto.Suggestions, error) {
	req := geoEntity.GeocodeRequest{
		Lat: request.Lat,
		Lng: request.Lng,
	}

	geocodeResponse, err := g.GeoService.PrepareGeocodeRequest(req)
	if err != nil {
		return nil, err
	}

	var s []*proto.Suggestion
	for _, val := range geocodeResponse.Suggestions {
		s = append(s, &proto.Suggestion{
			Value: val.Value,
			Data:  val.Data,
		})
	}

	return &proto.Suggestions{Suggestions: s}, nil

}

func (g *GeoServiceGRPC) PrepareSearchRequest(c context.Context, request *proto.SearchRequest) (*proto.SearchResponse, error) {
	req := geoEntity.SearchRequest{Query: request.Query}

	searchResponse, err := g.GeoService.PrepareSearchRequest(req)
	if err != nil {
		return nil, err
	}

	var a []*proto.Address
	for _, val := range searchResponse.Addresses {
		a = append(a, &proto.Address{
			Lat:    val.Lat,
			Lon:    val.Lon,
			Result: val.Result,
		})
	}

	return &proto.SearchResponse{Addresses: a}, nil
}
