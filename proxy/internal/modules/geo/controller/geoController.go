package controller

import (
	"encoding/json"
	"geoservicegrpc/proxy/internal/entities/geoEntity"
	"geoservicegrpc/proxy/internal/infrastructure/responder"
	"geoservicegrpc/proxy/internal/modules/geo/service"
	"geoservicegrpc/proxy/metrics"
	"net/http"
	"time"
)

type GeoController struct {
	service service.GeoServicer
	respond responder.Responder
	//cacheRedis *redis.Client
}

func NewGeoController(service service.GeoServicer, respond responder.Responder) *GeoController {
	return &GeoController{
		service: service,
		respond: respond,
	}
}

func (g *GeoController) SearchAddressHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	defer func() {
		metrics.SearchAddressRequestCount.Inc()
		metrics.SearchAddressDurationCount.Observe(float64(time.Since(start)))
	}()

	var requestByUser geoEntity.SearchRequest
	err := json.NewDecoder(r.Body).Decode(&requestByUser)
	if err != nil {
		g.respond.ErrorBadRequest(w, err)
	}

	searchResponse, err := g.service.PrepareSearchRequest(requestByUser)
	if err != nil {
		g.respond.ErrorInternal(w, err)
	}

	g.respond.OutputJSON(w, searchResponse)
}

func (g *GeoController) GeocodeAddressHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	defer func() {
		metrics.GeocodeRequestCount.Inc()
		metrics.GeocodeDurationCount.Observe(float64(time.Since(start)))
	}()

	var request geoEntity.GeocodeRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		g.respond.ErrorBadRequest(w, err)
	}

	suggestions, err := g.service.PrepareGeocodeRequest(request)
	if err != nil {
		g.respond.ErrorInternal(w, err)
	}

	addresses := &geoEntity.GeocodeResponse{}
	for _, adr := range suggestions.Suggestions {
		addresses.Addresses = append(addresses.Addresses,
			&geoEntity.Address{
				Lat:    adr.Data["geo_lat"], //TODO тут убрал указатель (было *adr.Data["geo_lat"])
				Lon:    adr.Data["geo_lon"], //TODO тут убрал указатель (было *adr.Data["geo_lon"])
				Result: adr.Value,
			})
	}

	g.respond.OutputJSON(w, addresses)
}
