package repository

import (
	"database/sql"
	"fmt"
	"geoservicegrpc/proxy/internal/entities/geoEntity"
)

type GeoStorage struct {
	db *sql.DB
}

func NewGeoStorage(db *sql.DB) GeoStorager {
	return &GeoStorage{db: db}
}

func (g *GeoStorage) AddAddressQueryLink(query, lat, lon string) error {

	tx, err := g.db.Begin() //начинаем транзакцию
	if err != nil {
		return err
	}
	defer tx.Rollback() //откатываем транзакцию в случае ошибки

	prepareAddr := "INSERT INTO search (query) values ($1)"
	if _, err = tx.Exec(prepareAddr, query); err != nil { //добавляем адрес из запроса пользователя в таблицу search
		return err
	}

	prepareCoord := "insert into address (lat,lon) values ($1,$2)"
	if _, err = tx.Exec(prepareCoord, lat, lon); err != nil { //добавляем координаты в таблицу address
		return err
	}

	//получаем последние id из таблиц address search
	var searchId int
	var adrId int
	prepareIDs := "select id from search order by id desc limit 1"
	if err = tx.QueryRow(prepareIDs).Scan(&searchId); err != nil {
		return err
	}

	prepareIDa := "select id from address order by id desc limit 1"
	if err = tx.QueryRow(prepareIDa).Scan(&adrId); err != nil {
		return err
	}

	//создаёмсвязь в таблице history_query
	prepareLink := "insert into history_query (search_id, address_id) values ($1,$2)"
	if _, err = tx.Exec(prepareLink, searchId, adrId); err != nil {
		return err
	}

	//если всё успешно, фиксируем транзакцию
	if err = tx.Commit(); err != nil {
		return err
	}

	return nil
}

func (g *GeoStorage) FindWithLevenshtein(query string) (*geoEntity.Address, error) {
	rows, err := g.db.Query(`
	SELECT address.lat, address.lon
	FROM history_query
	JOIN search ON search.id = history_query.search_id
	JOIN address ON history_query.address_id = address.id
	WHERE levenshtein(search.query, $1) <= LENGTH($1) * 0.3;
		`, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	//если не найдено совпадений  и была ошибка, то возвращаем ошибку
	//если не найдено совпадений и при этом не было ошибки, то возвращаем nil и в адресе структуры и в ошибке
	// соответственно уровнем выше, если у нас нет ошибки и при этом нет адреса на структуру - значит записи не найдено
	for !rows.Next() {
		if err = rows.Err(); err != nil {
			fmt.Println("Error iterating over rows:", err)
			return nil, err
		}
		return nil, nil
	}
	var adr geoEntity.Address
	if err = rows.Scan(&adr.Lat, &adr.Lon); err != nil {
		return nil, err
	}
	return &adr, nil
}
