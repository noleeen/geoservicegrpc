package repository

import "geoservicegrpc/proxy/internal/entities/geoEntity"

type GeoStorager interface {
	AddAddressQueryLink(query, lat, lon string) error
	FindWithLevenshtein(query string) (*geoEntity.Address, error)
}
