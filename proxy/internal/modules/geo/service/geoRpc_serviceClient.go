package service

import (
	"fmt"
	"geoservicegrpc/proxy/config"
	"geoservicegrpc/proxy/internal/entities/geoEntity"
	"log"
	"net/rpc"
)

type GeoServicRPC struct {
	client *rpc.Client
}

func NewGeoServicRPC(conf config.Config) *GeoServicRPC {

	client, err := rpc.Dial("tcp", fmt.Sprintf("%s:%s", conf.GeoRPC.Host, conf.GeoRPC.Port))
	if err != nil {
		log.Fatal("| rpc.Dial |error init rpc client ", err)
	}
	log.Println("rpc client init")

	return &GeoServicRPC{client: client}
}

func (g *GeoServicRPC) PrepareGeocodeRequest(in geoEntity.GeocodeRequest) (*geoEntity.Suggestions, error) {
	var out geoEntity.Suggestions
	err := g.client.Call("GeoServiceRPC.PrepareGeocodeRequest", in, &out)
	if err != nil {
		log.Println("error GeoServicRPC|PrepareGeocodeRequest:", err)
		return nil, err
	}

	return &out, nil
}
func (g *GeoServicRPC) PrepareSearchRequest(in geoEntity.SearchRequest) (*geoEntity.SearchResponse, error) {
	var out geoEntity.SearchResponse
	err := g.client.Call("GeoServiceRPC.PrepareSearchRequest", in, &out)
	if err != nil {
		log.Println("error GeoServicRPC|PrepareSearchRequest:", err)
		return nil, err
	}
	return &out, nil
}
