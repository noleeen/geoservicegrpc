package service

import (
	"context"
	"fmt"
	"geoservicegrpc/proxy/config"
	"geoservicegrpc/proxy/internal/entities/geoEntity"
	"geoservicegrpc/proxy/rpc/grpc/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

type GeoServicGRPC struct {
	client proto.GeoServiceRPCClient
}

func NewGeoServicGRPC(conf config.Config) *GeoServicGRPC {

	clientConn, err := grpc.Dial(fmt.Sprintf("%s:%s", conf.GeoRPC.Host, conf.GeoRPC.Port), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("| grpc.Dial | error init grpc client ", err)
	}

	client := proto.NewGeoServiceRPCClient(clientConn)
	log.Println("grpc client init")

	return &GeoServicGRPC{client: client}
}

func (g *GeoServicGRPC) PrepareGeocodeRequest(in geoEntity.GeocodeRequest) (*geoEntity.Suggestions, error) {
	// Преобразование geoEntity.GeocodeRequest в proto.GeocodeRequest
	req := &proto.GeocodeRequest{
		Lat: in.Lat,
		Lng: in.Lng,
	}

	// Вызов метода GeocodeSearch на сервере
	geocodeResponse, err := g.client.PrepareGeocodeRequest(context.Background(), req) //ПЛОХО!!! TODO контекст изначально должен быть в методах сервиса!!!
	if err != nil {
		log.Println("|g.client.PrepareGeocodeRequest| error:", err)
		return nil, err
	}

	// Преобразование proto.Suggestions в geoEntity.Suggestions
	var s []*geoEntity.Suggestion
	for _, val := range geocodeResponse.Suggestions {
		s = append(s, &geoEntity.Suggestion{
			Value: val.Value,
			Data:  val.Data,
		})
	}

	return &geoEntity.Suggestions{Suggestions: s}, nil
}
func (g *GeoServicGRPC) PrepareSearchRequest(in geoEntity.SearchRequest) (*geoEntity.SearchResponse, error) {
	// Преобразование geoEntity.SearchRequest в proto.SearchRequest
	req := &proto.SearchRequest{Query: in.Query}

	// Вызов метода SearchAddress на сервере
	searchResponse, err := g.client.PrepareSearchRequest(context.Background(), req) //ПЛОХО!!! TODO контекст изначально должен быть в методах сервиса!!!
	if err != nil {
		log.Println("|g.client.PrepareSearchRequest| error:", err)
		return nil, err
	}

	// Преобразование proto.SearchResponse в geoEntity.SearchResponse
	var a []*geoEntity.Address

	for _, val := range searchResponse.Addresses {
		a = append(a, &geoEntity.Address{
			Lat:    val.Lat,
			Lon:    val.Lon,
			Result: val.Result,
		})
	}

	return &geoEntity.SearchResponse{Addresses: a}, nil
}
