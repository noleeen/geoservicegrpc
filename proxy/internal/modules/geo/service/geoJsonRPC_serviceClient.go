package service

import (
	"fmt"
	"geoservicegrpc/proxy/config"
	"geoservicegrpc/proxy/internal/entities/geoEntity"
	"log"
	"net/rpc"
	"net/rpc/jsonrpc"
)

type GeoServicJsonRPC struct {
	client *rpc.Client
}

func NewGeoServicJsonRPC(conf config.Config) *GeoServicJsonRPC {

	client, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", conf.GeoRPC.Host, conf.GeoRPC.Port))
	if err != nil {
		log.Fatal("| jsonrpc.Dial |error init jsonrpc client ", err)
	}
	log.Println("jsonrpc client init")

	return &GeoServicJsonRPC{client: client}
}

func (g *GeoServicJsonRPC) PrepareGeocodeRequest(in geoEntity.GeocodeRequest) (*geoEntity.Suggestions, error) {
	var out geoEntity.Suggestions
	err := g.client.Call("GeoServiceRPC.PrepareGeocodeRequest", in, &out)
	if err != nil {
		log.Println("error GeoServicJsonRPC|PrepareGeocodeRequest:", err)
		return nil, err
	}

	return &out, nil
}
func (g *GeoServicJsonRPC) PrepareSearchRequest(in geoEntity.SearchRequest) (*geoEntity.SearchResponse, error) {
	var out geoEntity.SearchResponse
	err := g.client.Call("GeoServiceRPC.PrepareSearchRequest", in, &out)
	if err != nil {
		log.Println("error GeoServicJsonRPC|PrepareSearchRequest:", err)
		return nil, err
	}
	return &out, nil
}
