package service

import (
	"geoservicegrpc/proxy/internal/entities/geoEntity"
)

type GeoServicer interface {
	PrepareGeocodeRequest(coordinates geoEntity.GeocodeRequest) (*geoEntity.Suggestions, error)
	PrepareSearchRequest(query geoEntity.SearchRequest) (*geoEntity.SearchResponse, error)
}
