package modules

import (
	"geoservicegrpc/proxy/internal/infrastructure/responder"
	acontroller "geoservicegrpc/proxy/internal/modules/auth/controller"
	gcontroller "geoservicegrpc/proxy/internal/modules/geo/controller"
	"geoservicegrpc/proxy/internal/modules/geo/service"
)

type Controllers struct {
	Auth acontroller.AuthControllerer
	Geo  gcontroller.GeoControllerer
}

func NewControllers(services *Services, serviceRPC service.GeoServicer, respond responder.Responder) *Controllers {
	return &Controllers{
		Auth: acontroller.NewAuthController(services.Auth, respond),
		Geo:  gcontroller.NewGeoController(serviceRPC, respond),
	}

}
