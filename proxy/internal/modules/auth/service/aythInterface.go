package service

import "geoservicegrpc/proxy/internal/entities/authEntity"

type AuthServicer interface {
	CheckRequestRegisterUser(user authEntity.User) error
	AddUser(user authEntity.User) error
	CheckRequestLoginUser(user authEntity.User) error
	GetToken(user authEntity.User) (string, error)
}
