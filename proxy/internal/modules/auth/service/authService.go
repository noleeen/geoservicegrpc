package service

import (
	"errors"
	"fmt"
	"geoservicegrpc/proxy/internal/entities/authEntity"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/jwtauth"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type AuthService struct {
	Storage   *authEntity.StorageAuth
	tokenAuth *jwtauth.JWTAuth
}

func NewAuthService(storage *authEntity.StorageAuth, token *jwtauth.JWTAuth) AuthServicer {
	return &AuthService{
		Storage:   storage,
		tokenAuth: token,
	}
}

// проверяем не пустой ли запрос и нет ли такого пользователя уже в базе
func (a *AuthService) CheckRequestRegisterUser(user authEntity.User) error {
	if user.Name == "" || user.Password == "" {
		return errors.New("name or password is empty")
	}

	if _, ok := a.Storage.UsersMap[user.Name]; ok {
		return fmt.Errorf("user with name %s already exist", user.Name)
	}

	return nil
}

func (a *AuthService) AddUser(user authEntity.User) error {
	hashPass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	a.Storage.UsersMap[user.Name] = authEntity.User{
		Name:     user.Name,
		Password: string(hashPass),
	}
	return nil
}

// проверяем не пустой ли запрос, ищем пользователя в базе и проверяем подходит ли пароль
func (a *AuthService) CheckRequestLoginUser(user authEntity.User) error {
	if user.Name == "" || user.Password == "" {
		return errors.New("name or password is empty")
	}

	checkUser, ok := a.Storage.UsersMap[user.Name]
	if !ok {
		return errors.New("user not found")
	}

	err := bcrypt.CompareHashAndPassword([]byte(checkUser.Password), []byte(user.Password))
	if err != nil {
		return fmt.Errorf("incorrect password: %v", err)
	}
	return nil
}

func (a *AuthService) GetToken(user authEntity.User) (string, error) {
	_, tokenStr, err := a.tokenAuth.Encode(jwt.MapClaims{
		"name": user.Name,
		"exp":  time.Now().Add(time.Hour * 24).Unix(),
	})
	if err != nil {
		return "", err
	}
	return tokenStr, nil
}
