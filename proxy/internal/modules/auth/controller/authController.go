package controller

import (
	"encoding/json"
	"geoservicegrpc/proxy/internal/entities/authEntity"
	"geoservicegrpc/proxy/internal/infrastructure/responder"
	"geoservicegrpc/proxy/internal/modules/auth/service"
	"geoservicegrpc/proxy/metrics"
	"net/http"
	"time"
)

type AuthController struct {
	service service.AuthServicer
	respond responder.Responder
}

func NewAuthController(service service.AuthServicer, respond responder.Responder) *AuthController {
	return &AuthController{
		service: service,
		respond: respond,
	}
}

func (a *AuthController) Register(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	defer func() {
		metrics.RegisterRequestCount.Inc()
		metrics.RegisterDurationCount.Observe(float64(time.Since(start)))
	}()

	var registerRequest authEntity.User

	err := json.NewDecoder(r.Body).Decode(&registerRequest)
	if err != nil {
		a.respond.ErrorBadRequest(w, err)
		return
	}

	//проверяем не пустой ли запрос и не существует ли такое имя пользователя уже в базе
	if err = a.service.CheckRequestRegisterUser(registerRequest); err != nil {
		a.respond.ErrorBadRequest(w, err)
		return
	}

	//добавляем пользователя
	if err = a.service.AddUser(registerRequest); err != nil {
		a.respond.ErrorInternal(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte("user successfully created"))
}

func (a *AuthController) Login(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	defer func() {
		metrics.LoginRequestCount.Inc()
		metrics.LoginDurationCount.Observe(float64(time.Since(start)))
	}()

	var loginRequest authEntity.User

	err := json.NewDecoder(r.Body).Decode(&loginRequest)
	if err != nil {
		a.respond.ErrorBadRequest(w, err)
		return
	}

	//проверяем не пустой ли запрос, ищем пользователя в базе и проверяем подходит ли пароль
	if err = a.service.CheckRequestLoginUser(loginRequest); err != nil {
		a.respond.ErrorBadRequest(w, err)
		return
	}

	token, err := a.service.GetToken(loginRequest)
	if err != nil {
		a.respond.ErrorInternal(w, err)
		return
	}

	a.respond.OutputJSON(w, token)
}
