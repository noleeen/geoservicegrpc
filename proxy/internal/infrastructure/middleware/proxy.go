package middleware

import (
	"fmt"
	"geoservicegrpc/proxy/static"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
)

type ReverseProxy struct {
	host string
	port string
}

func NewReverseProxy(host, port string) *ReverseProxy {
	return &ReverseProxy{
		host: host,
		port: port,
	}
}

//func (rp *ReverseProxy) ReverseProxy(next http.Handler) http.Handler {
//	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//		if !strings.HasPrefix(r.URL.Path, "/api/") && r.URL.Path != "/static/swagger.json" && r.URL.Path != "/swagger" {
//			newURL, err := url.Parse(fmt.Sprintf("http://%s:%s", rp.host, rp.port))
//			if err != nil {
//				log.Printf("Error parsing URL: %v\n", err)
//			}
//			reverseProxy := httputil.NewSingleHostReverseProxy(newURL)
//
//			reverseProxy.ServeHTTP(w, r)
//		} else {
//			next.ServeHTTP(w, r)
//		}
//
//	})
//}

func (rp *ReverseProxy) ReverseProxy(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !strings.HasPrefix(r.URL.Path, "/api/") &&
			!strings.HasPrefix(r.URL.Path, "/debug/") &&
			!strings.HasPrefix(r.URL.Path, "/metrics") {
			newUrl, err := url.Parse(fmt.Sprintf("http://%s:%s", rp.host, rp.port))
			if err != nil {
				log.Println(err)
			}
			proxy := httputil.NewSingleHostReverseProxy(newUrl)
			r.Host = "hugo:1313"

			if strings.HasPrefix(r.URL.Path, "/swagger") {
				static.SwaggerUI(w, r)
				return
			}

			if strings.HasPrefix(r.URL.Path, "/static") {
				http.ServeFile(w, r, "/static/swagger.json")
				return
			}

			proxy.ServeHTTP(w, r)
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
