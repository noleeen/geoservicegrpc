package server

import (
	"context"
	"fmt"
	"geoservicegrpc/proxy/config"
	"log"
	"net"
	"net/rpc"
)

type ServerRPC struct {
	srv  *rpc.Server
	conf config.ServerRPC
}

func NewServerRPC(conf config.ServerRPC, srv *rpc.Server) Server {
	return &ServerRPC{
		srv:  srv,
		conf: conf,
	}
}

func (s *ServerRPC) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		var l net.Listener
		l, err = net.Listen("tcp", fmt.Sprintf(":%s", s.conf.Port))
		if err != nil {
			log.Println("error Serve|ServerRPC:", err)
			chErr <- err
		}

		log.Println("rpc server started on port:", s.conf.Port)
		var conn net.Conn
		for {
			select {
			case <-ctx.Done():
				log.Println("rpc: stopping server")
				return
			default:
				conn, err = l.Accept()
				if err != nil {
					log.Println("rpc: net tcp accept error:", err)
				}
				go s.srv.ServeConn(conn)
			}
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
	}
	return err
}
