package server

import (
	"context"
	"log"
	"net/http"
	"time"
)

type HttpServer struct {
	srv *http.Server
}

func NewHttpServer(serv *http.Server) Server {
	return &HttpServer{srv: serv}
}

func (s *HttpServer) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		log.Println("server started on port 8080")
		if err = s.srv.ListenAndServe(); err != http.ErrServerClosed {
			log.Println("http listen and serve error", err)
			chErr <- err
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
	}

	ctxShutdown, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	err = s.srv.Shutdown(ctxShutdown)

	return err
}
