package server

import (
	"context"
	"fmt"
	"geoservicegrpc/proxy/config"
	"log"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
)

type ServerJsonRPC struct {
	srv  *rpc.Server
	conf config.ServerRPC
}

func NewServerJsonRPC(conf config.ServerRPC, srv *rpc.Server) Server {
	return &ServerJsonRPC{
		conf: conf,
		srv:  srv,
	}
}

func (s *ServerJsonRPC) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		var l net.Listener
		l, err = net.Listen("tcp", fmt.Sprintf(":%s", s.conf.Port))
		if err != nil {
			log.Println("error Serve|ServerJsonRPC:", err)
			chErr <- err
		}

		log.Println("Jsonrpc server started on port:", s.conf.Port)
		var conn net.Conn
		for {
			select {
			case <-ctx.Done():
				log.Println("Jsonrpc: stopping server")
				return
			default:
				conn, err = l.Accept()
				if err != nil {
					log.Println("jsonrpc: net tcp accept error:", err)
				}
				go s.srv.ServeCodec(jsonrpc.NewServerCodec(conn))
			}
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
	}
	return err
}
