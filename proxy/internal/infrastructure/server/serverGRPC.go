package server

import (
	"context"
	"fmt"
	"geoservicegrpc/proxy/config"
	"google.golang.org/grpc"

	"log"
	"net"
)

type ServerGRPC struct {
	srv  *grpc.Server
	conf config.ServerRPC
}

func NewServerGRPC(conf config.ServerRPC, srv *grpc.Server) Server {
	return &ServerGRPC{
		conf: conf,
		srv:  srv,
	}
}

func (s *ServerGRPC) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		var l net.Listener
		l, err = net.Listen("tcp", fmt.Sprintf(":%s", s.conf.Port))
		if err != nil {
			log.Println("error Serve|ServerGRPC:", err)
			chErr <- err
		}

		log.Println("grpc server started on port:", s.conf.Port)

		if err = s.srv.Serve(l); err != nil {
			chErr <- err
		}

	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
		s.srv.GracefulStop()
	}
	return err
}
