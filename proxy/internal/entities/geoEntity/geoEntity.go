package geoEntity

// -------------/address/search--------------
type SearchRequest struct {
	Query string `json:"query"`
}

type SearchResponse struct {
	Addresses []*Address `json:"addresses"`
}

type Address struct {
	Lat    string `json:"lat"`
	Lon    string `json:"lon"`
	Result string `json:"result"`
}

//-------------/address/geocode--------------

type GeocodeRequest struct {
	Lat string `json:"lat"`
	Lng string `json:"lng"`
}

type Adapter struct {
	Lat           string `json:"lat"`
	Lon           string `json:"lon"`
	Radius_meters string `json:"radius_Meters"`
}

type GeocodeResponse struct {
	Addresses []*Address `json:"addresses"`
}

type Suggestions struct {
	Suggestions []*Suggestion `json:"suggestions"`
}

type Suggestion struct {
	Value string            `json:"value"`
	Data  map[string]string `json:"data"` // //TODO тут убрал указатель  (было map[string]*string)
}
