CREATE TABLE search(
    id SERIAL PRIMARY KEY,
    query TEXT NOT NULL
);

CREATE TABLE address(
    id SERIAL PRIMARY KEY,
    lat TEXT NOT NULL,
    lon TEXT NOT NULL
);

CREATE TABLE history_query(
    id SERIAL PRIMARY KEY,
    search_id INT REFERENCES search(id),
    address_id INT REFERENCES address(id)
);

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;

