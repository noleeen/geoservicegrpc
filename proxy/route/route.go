package route

import (
	"geoservicegrpc/proxy/internal/infrastructure/middleware"
	"geoservicegrpc/proxy/internal/modules"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth"
	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
	"net/http/pprof"
)

func NewApiRouter(r *chi.Mux, controllers *modules.Controllers, token *jwtauth.JWTAuth) http.Handler {

	proxy := middleware.NewReverseProxy("hugo", "1313")
	r.Use(proxy.ReverseProxy)

	r.Handle("/metrics", promhttp.Handler())

	r.Route("/api", func(r chi.Router) {
		r.Get("/*", HelloFromApi)

		r.Post("/login", controllers.Auth.Login)
		r.Post("/register", controllers.Auth.Register)
		r.Route("/address", func(r chi.Router) {
			r.Use(jwtauth.Verifier(token))
			r.Use(jwtauth.Authenticator)
			// по адресу http://localhost:8080/address/search/ пишем в поисковой строке приблизительный адрес,
			//с помощью geoservice.SearchAddressHandler подирается предполагаемый адрес и показывается на карте.
			//нажимаем на маркер на карте и с помощью geoservice.GeocodeAddressHandler нам показывается список из точных адресов в радиусе 75 метров
			r.Post("/search", controllers.Geo.SearchAddressHandler)
			r.Post("/geocode", controllers.Geo.GeocodeAddressHandler)
		})
	})

	r.Route("/debug", func(r chi.Router) {
		r.Group(func(r chi.Router) {
			r.Use(jwtauth.Verifier(token))
			r.Use(jwtauth.Authenticator)
			r.Group(func(r chi.Router) {
				r.HandleFunc("/pprof", pprof.Index)
				r.HandleFunc("/cmdline", pprof.Cmdline)
				r.HandleFunc("/profile", pprof.Profile)
				r.HandleFunc("/symbol", pprof.Symbol)
				r.HandleFunc("/trace", pprof.Trace)
				r.Handle("/allocs", pprof.Handler("allocs"))
				r.Handle("/block", pprof.Handler("block"))
				r.Handle("/goroutine", pprof.Handler("goroutine"))
				r.Handle("/heap", pprof.Handler("heap"))
				r.Handle("/mutex", pprof.Handler("mutex"))
				r.Handle("/threadcreate", pprof.Handler("threadcreate"))
			})
		})
	})

	return r
}

func HelloFromApi(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello from API"))
}
