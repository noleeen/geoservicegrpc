package workers

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	testNode = &Node{
		ID:   1,
		Name: "name",
		Form: []string{"(", ")"},
	}
	testNode2 = &Node{
		ID:   2,
		Name: "name2",
		Form: nil,
	}
	testGraph = NewGraph()
)

func TestGraph_AddNode(t *testing.T) {

	type args struct {
		node *Node
	}
	tests := []struct {
		name string
		args args
		want *Node
	}{
		{
			name: "addNode",
			args: args{node: testNode},
			want: &Node{
				ID:   1,
				Name: "name",
				Form: []string{"(", ")"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testGraph.AddNode(tt.args.node)
			assert.Equal(t, tt.want, testGraph.Nodes[0])

		})
	}
}

func TestGraph_AddEdge(t *testing.T) {
	testEdge := &Edge{
		From: testNode,
		To:   testNode2,
	}

	type args struct {
		from *Node
		to   *Node
	}
	tests := []struct {
		name string
		args args
		want *Graph
	}{
		{
			name: "",
			args: args{
				from: testNode,
				to:   testNode2,
			},
			want: &Graph{Edges: []*Edge{testEdge}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testGraph.AddEdge(tt.args.from, tt.args.to)
			assert.Equal(t, tt.want, testGraph)
		})
	}
}

func TestGraph_CreateRandGraph(t *testing.T) {
	type args struct {
		from int
		to   int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "",
			args: args{
				from: 20,
				to:   25,
			},
			want: []int{20, 21, 22, 23, 24, 25},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testGraph.CreateRandGraph(tt.args.from, tt.args.to)
			assert.NotEmpty(t, testGraph.Nodes, "CreateRandGraph(%v, %v)", tt.args.from, tt.args.to)
		})
	}
}
