package workers

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAVLTree_Insert(t1 *testing.T) {
	type fields struct {
		Root *node
	}
	type args struct {
		key int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "",
			fields: fields{Root: newNode(1)},
			args:   args{key: 2},
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &AVLTree{
				Root: tt.fields.Root,
			}
			t.Insert(tt.args.key)
			assert.NotNil(t1, t.Root.Right)
		})
	}
}

func Test_height(t *testing.T) {
	type args struct {
		node *node
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "",
			args: args{node: &node{
				Key:    1,
				Height: 7,
				Left:   nil,
				Right:  nil,
			}},
			want: 7,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, height(tt.args.node), "height(%v)", tt.args.node)
		})
	}
}

func Test_max(t *testing.T) {
	type args struct {
		a int
		b int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "",
			args: args{
				a: 1,
				b: 2,
			},
			want: 2,
		},
		{
			name: "",
			args: args{
				a: 1,
				b: -2,
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, max(tt.args.a, tt.args.b), "max(%v, %v)", tt.args.a, tt.args.b)
		})
	}
}

func TestGenerateTree(t *testing.T) {
	type args struct {
		count int
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "",
			args: args{count: 2},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			avlTree := GenerateTree(tt.args.count)
			assert.NotNil(t, avlTree.Root.Key, "GenerateTree(%v)", tt.args.count)
		})
	}
}
