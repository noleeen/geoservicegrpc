package metrics

import "github.com/prometheus/client_golang/prometheus"

func init() {
	prometheus.MustRegister(SearchAddressRequestCount)
	prometheus.MustRegister(SearchAddressDurationCount)

	prometheus.MustRegister(GeocodeRequestCount)
	prometheus.MustRegister(GeocodeDurationCount)

	prometheus.MustRegister(RegisterRequestCount)
	prometheus.MustRegister(RegisterDurationCount)

	prometheus.MustRegister(LoginRequestCount)
	prometheus.MustRegister(LoginDurationCount)

	prometheus.MustRegister(ReadCache)
	prometheus.MustRegister(WriteCache)

	prometheus.MustRegister(ReadDB)
	prometheus.MustRegister(WriteDB)

	prometheus.MustRegister(RequestToApiDadata)

}

var (
	SearchAddressRequestCount = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "SearchAddressRequestCount",
		Help: "считает колтчество запросов к SearchAddress",
	})
	SearchAddressDurationCount = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "SearchAddressDurationCount",
		Help: "считает время выполнения запроса к SearchAddress",
	})

	GeocodeRequestCount = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "GeocodeRequestCount",
		Help: "считает колтчество запросов к Geocode",
	})
	GeocodeDurationCount = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "GeocodeDurationCount",
		Help: "считает время выполнения запроса к Geocode",
	})

	RegisterRequestCount = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "RegisterRequestCount",
		Help: "считает колтчество запросов к Register",
	})
	RegisterDurationCount = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "RegisterDurationCount",
		Help: "считает время выполнения запроса к Register",
	})

	LoginRequestCount = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "LoginRequestCount",
		Help: "считает колтчество запросов к Login",
	})
	LoginDurationCount = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "LoginDurationCount",
		Help: "считает время выполнения запроса к Login",
	})

	ReadCache = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "ReadCache",
		Help: "считает время запроса чтения к кэшу",
	})
	WriteCache = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "WriteCache",
		Help: "считает время запроса записи в кэш",
	})

	ReadDB = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "ReadDB",
		Help: "считает время запроса чтения к базе данных",
	})
	WriteDB = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "WriteDB",
		Help: "считает время запроса записи в базу данных",
	})

	RequestToApiDadata = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "RequestToApiDadata",
		Help: "считает время запроса к внешней API",
	})
)
