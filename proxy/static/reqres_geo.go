package static

import (
	"geoservicegrpc/proxy/internal/entities/geoEntity"
)

//swagger:route POST /api/register registrationUser RegisterRequest
// Регистрация нового пользователя
// security:
// - basic
// Responses:
// 200:

//swagger:parameters RegisterRequest
type RegisterRequest struct {
	//R
	//in:body
	//required: true
	//example: {"name":"testUser", "password":"testPass"}
	Reg string
}

// swagger:route POST /api/login loginUser LoginRequest
// Аутентификация пользователя
// security:
// - basic
//Responses:
// 200:

//swagger:parameters LoginRequest
type LoginRequest struct {
	//L
	//in:body
	//required: true
	//example: {"name":"testUser", "password":"testPass"}
	Body string
}

// swagger:route POST /api/address/search searchAddress SearchRequest
// Поиск адреса.
// Security:
// - Bearer: []
// Parameters:
//		+ name: SearchRequest
//		 description: пример запроса: {"query":"москва"}
//		  in: body
//		  type: SearchRequest
// Responses:
// 200: SearchResponse
// 400: description: Bad request
// 500: description: Internal server error

// SearchRequest security:
// - Bearer: []
//
//swagger:model SearchRequest
type SearchRequest struct {
	// in: body
	// required: true
	// properties:
	//   query:
	//     type: string
	Query string `json:"query"`
}

//swagger:model SearchResponse
type SearchResponse struct {
	// required: true
	// type: object
	// properties:
	//		addresses:
	//			type: array
	//			items:
	//				type: Address
	Addresses []*geoEntity.Address `json:"addresses"`
}

// swagger:route POST /api/address/geocode geolocate GeocodeRequest
// Поиск ближайшших адресов по координатам.
// Security:
// - Bearer: []
// Parameters:
//		+ name: GeocodeRequest
//		description: пример запроса: {"lat":"61.9484","lon":"130.2965"}
//		in: body
//		type: GeocodeRequest
// Responses:
// 200: GeocodeResponse
// 400: description: Bad request
// 500: description: Internal server error

// GeocodeRequest security:
// - Bearer: []
// swagger:model GeocodeRequest
type GeocodeRequest struct {
	//required: true
	//type: object
	//properties:
	//	coordinates:
	//		type: string
	Coordinates geoEntity.GeocodeRequest
}

// swagger:model GeocodeResponse
type GeocodeResponse struct {
	// required: true
	// type: object
	// properties:
	//     addresses:
	//         type: array
	//         items:
	//                type: Address
	Addresses []*geoEntity.Address `json:"addresses"`
}
